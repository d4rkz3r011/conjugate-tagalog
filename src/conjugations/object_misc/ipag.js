var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    doVowel: function(word) {
        cmd = "ipag-" + word;
        past = "ipinag-" + word;
        present = "ipinag-" + word.substring(0,1) + word;
        future = "ipag-" + word.substring(0,1) + word;
    },

    ipa: function(word) {
        cmd = "ipag" + word;
        past = "ipinag" + word;
        present = "ipinag" + word.substring(0,2) + word;
        future = "ipag" + word.substring(0,2) + word;
    },
    
    conjugate: function(word) {
        if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else {
            functions.ipa(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;