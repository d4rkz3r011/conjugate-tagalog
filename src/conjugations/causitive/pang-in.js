var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    doVowel: function(word) {
        cmd = "pang" + word + "in";
        past = "pinang" + word;
        present = "pinang" + word.substring(0,1) + "ng" + word;
        future = "pang" + word.substring(0,1) + "ng" + word + "in";
    },

    isPam: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'b' || letter == 'p') {
            return true;
        }
        return false;
    },

    pam: function(word) {
        cmd = "pam" + word.substring(1) + "in";
        past = "pinam" + word.substring(1);
        present = "pinam" + word.substring(1,2) + "m" + word.substring(1);
        future = "pam" + word.substring(1,2) + "m" + word.substring(1) + "in";
    },

    isPan: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'd' || letter == 's' || letter == 'l' || letter == 't' || letter == 'r') {
            return true;
        }
        return false;
    },

    pan: function(word) {   
        let letter = word.substring(0,1);

        if (functions.includes(word)) { // Keep the first letter in conjugating
            if (letter == 'd') { // If the words starts with a d, keep the d
                cmd = "pan" + word + "in";
                past = "pinan" + word;
                present = "pinan" + word.substring(0,2) + "r" + word.substring(1); // d -> r when between 2 vowels
                future = "pan" + word.substring(0,2) + "r" + word.substring(1) + "in";
            }
            else { // If the word starts with a l, keep the l
                cmd = "pan" + word + "in";
                past = "pinan" + word;
                present = "pinan" + word.substring(0,2) + word;
                future = "pan" + word.substring(0,2) + word + "in";
            }
        }
        else {
            cmd = "pan" + word.substring(1) + "in";
            past = "pinan" + word.substring(1);
            present = "pinan" + word.substring(1,2) + "n" + word.substring(1);
            future = "pan" + word.substring(1,2) + "n" + word.substring(1) + "in";
        }
    },

    isK: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'k') {
            return true;
        }
        return false;
    },

    noK: function(word) {
        cmd = "pang" + word.substring(1) + "in";
        past = "pinang" + word.substring(1);
        present = "pinang" + word.substring(1,2) + "ng" + word.substring(1);
        future = "pang" + word.substring(1,2) + "ng" + word.substring(1) + "in";
    },

    pang_in: function(word) {
        cmd = "pag" + word + "in";
        past = "pinang" + word;
        present = "pinang" + word.substring(0,2) + word;
        future = "pag" + word.substring(0,2) + word + "in";
    },
    
    conjugate: function(word) {
        if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else if (functions.isPam(word)) {
            functions.pam(word);
        }
        else if (functions.isPan(word)) {
            functions.pan(word);
        }
        else if (functions.isK(word)) {
            functions.noK(word);
        }
        else {
            functions.pang_in(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;