# conjugate-tagalog

## About
I wrote this at BYU Provo for my Tagalog students as a little side project. This was mainly written for the 100 and 200 level classes so they could check their conjugations easily whenever and wherever.
This was written with in JavaScript using the Vue framework, because I wanted to try and apply IRL what I learned in the CS 260 Web Dev class I took before I graduated.
All the conjugations are hard coded and so are the exceptions.
I used the surge.sh (it's free!) npm package to make this website live, and it was very simple to do!

## Website url
http://conjugate-tagalog.surge.sh/

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
