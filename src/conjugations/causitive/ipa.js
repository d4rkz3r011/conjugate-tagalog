var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    ipa: function(word) {
        cmd = "ipa" + word;
        past = "ipina" + word;
        present = "ipinapa" + word;
        future = "ipapa" + word;
    },
    
    conjugate: function(word) {
        functions.ipa(word);
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;