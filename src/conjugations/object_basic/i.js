var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    doVowel: function(word) {
        cmd = "i" + word;
        past = "ini" + word;
        present = "ini" + word.substring(0,1) + word;
        future = "i" + word.substring(0,1) + word;
    },

    isHLY: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'h' || letter == 'l' || letter == 'y') {
            return true;
        }
        return false;
    },

    doHLY: function(word) {
        cmd = "i" + word;
        past = "ini" + word;
        present = "ini" + word.substring(0,2) + word;
        future = "i" + word.substring(0,2) + word;
    },
    
    i: function(word) {
        // TODO: Check if the second to last letter is 'o' and change to 'u'
        // TODO: Check when '-han' should be used
        cmd = "i" + word;
        past = "i" + word.substring(0,1) + "in" + word.substring(1);
        present = "i" + word.substring(0,1) + "in" + word.substring(1,2) + word;
        future = "i" + word.substring(0,2) + word;
    },
    
    conjugate: function(word) {
        if (functions.isHLY(word)) {
            functions.doHLY(word);
        }
        else if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else {
            functions.i(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;