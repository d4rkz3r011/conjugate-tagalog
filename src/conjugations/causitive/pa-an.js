var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    pa_an: function(word) {
        cmd = "pa" + word + "an";
        past = "pina" + word + "an";
        present = "pina" + word.substring(0,2) + word + "an";
        future = "pa" + word.substring(0,2) + word + "an";
    },
    
    conjugate: function(word) {
        functions.pa_an(word);
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;