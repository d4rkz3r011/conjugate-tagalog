var cmd = "";
var past = "";
var present = "";
var future = "";

var nonGlottal = [
    "bili",
    "pili",
    "sabi",
]

var noLastVowel = [
    "bigay",
    "lagay",
    "bukas",
]

const functions = {

    isLY: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'l' || letter == 'y') {
            return true;
        }
        return false;
    },

    doLY: function(word) {
        cmd = word + "an";
        past = "ni" + word + "an";
        present = "ni" + word.substring(0,2) + word + "an";
        future = word.substring(0,1) + word + "an";
    },

    doLYNoVowel: function(word) {
        let size = word.length;

        cmd = word.substring(0, size - 2) + word.substring(size - 1) + "an";
        past = "ni" + word.substring(0, size - 2) + word.substring(size - 1) + "an";
        present = "ni" + word.substring(0,2) + word.substring(0, size - 2) + word.substring(size - 1) + "an";
        future = word.substring(0, 2) + word.substring(0, size - 2) + word.substring(size - 1) + "an";
    },

    doLastVowel: function(word) {
        // make a check for when to put '-han'
        // make a check for words like 'bigyan'
        let size = word.length;
        cmd = word.substring(0, size - 2) + word.substring(size - 1) + "an";
        past = word.substring(0,1) + "in" + word.substring(1, size - 2) + word.substring(size - 1) + "an";
        present = word.substring(0,1) + "in" + word.substring(1,2) + word.substring(0, size - 2) + word.substring(size - 1) + "an";
        future = word.substring(0, 2) + word.substring(0, size - 2) + word.substring(size - 1) + "an";
    },


    doGlottal: function(word) {
        cmd = word + "an";
        past = "ni" + word + "an";
        present = "ni" + word.substring(0,2) + word + "an";
        future = word.substring(0,1) + word + "an";
    },
    
    an: function(word) {
        // TODO: Check if the second to last letter is 'o' and change to 'u'
        // TODO: make a check for when to put '-han'

        cmd = word + "an";
        past = word.substring(0,1) + "in" + word.substring(1) + "an";
        present = word.substring(0,1) + "in" + word.substring(1,2) + word + "an";
        future = word.substring(0,2) + word + "an";
    },
    
    conjugate: function(word) {
        if (noLastVowel.includes(word)) {
            if (functions.isLY(word)) {
                functions.doLYNoVowel(word);
            }
            else {
                functions.doLastVowel(word);
            }
        }
        else if (nonGlottal.includes(word)) {
            functions.doGlottal(word);
        }
        else if (functions.isLY(word)) {
            functions.doLY(word);
        }
        else {
            functions.an(word);
        }

        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;