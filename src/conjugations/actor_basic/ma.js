var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    ma: function(word) {
        cmd = "ma" + word
        past = "na" + word;
        present = "na" + word.substring(0,2) + word;
        future = "ma" + word.substring(0,2) + word;
    },
    
    conjugate: function(word) {
        functions.ma(word);
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;