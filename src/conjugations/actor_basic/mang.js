var cmd = "";
var past = "";
var present = "";
var future = "";

var exceptions = [
    "dukot",
    "linis",
]

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    doVowel: function(word) {
        cmd = "mang" + word
        past = "nang" + word;
        present = "nang" + word.substring(0,1) + "ng" + word;
        future = "mang" + word.substring(0,1) + "ng" + word;
    },

    isMam: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'b' || letter == 'p') {
            return true;
        }
        return false;
    },

    mam: function(word) {
        cmd = "mam" + word.substring(1);
        past = "nam" + word.substring(1);
        present = "nam" + word.substring(1,2) + "m" + word.substring(1);
        future = "mam" + word.substring(1,2) + "m" + word.substring(1);
    },

    isMan: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'd' || letter == 's' || letter == 'l' || letter == 't' || letter == 'r') {
            return true;
        }
        return false;
    },

    man: function(word) {   
        let letter = word.substring(0,1);

        if (exceptions.includes(word)) { // Keep the first letter in conjugating
            if (letter == 'd') { // If the words starts with a d, keep the d
                cmd = "man" + word;
                past = "nan" + word;
                present = "nan" + word.substring(0,2) + "r" + word.substring(1); // d -> r when between 2 vowels
                future = "man" + word.substring(0,2) + "r" + word.substring(1);
            }
            else { // If the word starts with a l, keep the l
                cmd = "man" + word;
                past = "nan" + word;
                present = "nan" + word.substring(0,2) + word;
                future = "man" + word.substring(0,2) + word;
            }
        }
        else {
            cmd = "man" + word.substring(1);
            past = "nan" + word.substring(1);
            present = "nan" + word.substring(1,2) + "n" + word.substring(1);
            future = "man" + word.substring(1,2) + "n" + word.substring(1);
        }
    },

    isK: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'k') {
            return true;
        }
        return false;
    },

    noK: function(word) {
        cmd = "mang" + word.substring(1);
        past = "nang" + word.substring(1);
        present = "nang" + word.substring(1,2) + "ng" + word.substring(1);
        future = "mang" + word.substring(1,2) + "ng" + word.substring(1);
    },

    mang: function(word) {
        cmd = "mang" + word
        past = "nang" + word;
        present = "nang" + word.substring(0,2) + word;
        future = "mang" + word.substring(0,2) + word;
    },
    
    conjugate: function(word) {
        if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else if (functions.isMam(word)) {
            functions.mam(word);
        }
        else if (functions.isMan(word)) {
            functions.man(word);
        }
        else if (functions.isK(word)) {
            functions.noK(word);
        }
        else {
            functions.mang(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;