var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    doVowel: function(word) {
        cmd = "pag-" + word + "in";
        past = "pinag-" + word;
        present = "pinag-" + word.substring(0,1) + word;
        future = "pag-" + word.substring(0,1) + word + "in";
    },

    pag_in: function(word) {
        cmd = "pag" + word + "in";
        past = "pinag" + word;
        present = "pinag" + word.substring(0,2) + word;
        future = "pag" + word.substring(0,2) + word + "in";
    },
    
    conjugate: function(word) {
        if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else {
            functions.pag_in(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;