var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    doVowel: function(word) {
        cmd = "pag-" + word + "an";
        past = "pinag-" + word + "an";
        present = "pinag-" + word.substring(0,1) + word + "an";
        future = "pag-" + word.substring(0,1) + word + "an";
    },

    pag_an: function(word) {
        cmd = "pag" + word + "an";
        past = "pinag" + word + "an";
        present = "pinag" + word.substring(0,2) + word + "an";
        future = "pag" + word.substring(0,2) + word + "an";
    },
    
    conjugate: function(word) {
        // TODO: Need to make the same checks as -an
        if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else {
            functions.pag_an(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;