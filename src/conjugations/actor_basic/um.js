var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    um: function(word) {
        cmd = word.substring(0,1) + "um" + word.substring(1);
        past = word.substring(0,1) + "um" + word.substring(1);
        present = word.substring(0,1) + "um" + word.substring(1,2) + word;
        future = word.substring(0,2) + word;
    },

    doVowel: function(word) {
        cmd = "um" + word;
        past = "um" + word;
        present = "um" + word.substring(0,1) + word;
        future = word.substring(0,1) + word;
    },
    
    conjugate: function(word) {
        if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else {
            functions.um(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;