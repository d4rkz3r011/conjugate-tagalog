var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    magpa: function(word) {
        cmd = "magpa" + word
        past = "nagpa" + word;
        present = "nagpapa" + word;
        future = "magpapa" + word;
    },
    
    conjugate: function(word) {
        functions.magpa(word);
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;