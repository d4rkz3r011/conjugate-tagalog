var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    doVowel: function(word) {
        cmd = "ika" + word;
        past = "ikina" + word;
        present = "ikina" + word.substring(0,1) + word;
        future = "ika" + word.substring(0,1) + word;
    },

    ika: function(word) {
        cmd = "ika" + word;
        past = "ikina" + word;
        present = "ikina" + word.substring(0,2) + word;
        future = "ika" + word.substring(0,2) + word;
    },
    
    conjugate: function(word) {
        if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else {
            functions.ika(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;