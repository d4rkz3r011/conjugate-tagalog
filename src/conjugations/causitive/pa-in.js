var cmd = "";
var past = "";
var present = "";
var future = "";

const functions = {
    pa_in: function(word) {
        cmd = "pa" + word + "in";
        past = "pina" + word;
        present = "pina" + word.substring(0,2) + word;
        future = "pa" + word.substring(0,2) + word + "in";
    },
    
    conjugate: function(word) {
        functions.pa_in(word);
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;