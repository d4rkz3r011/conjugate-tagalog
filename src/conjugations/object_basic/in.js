var cmd = "";
var past = "";
var present = "";
var future = "";

var nonGlottal = [
    "bili",
    "pili",
    "sabi",
    "huli",
]

var noLastLetter = [
    "gawa",
    "bili",
    "pili",
]

const functions = {
    isVowel: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'a' || letter == 'e' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u') {
            return true;
        }
        return false;
    },

    doVowel: function(word) {
        cmd = word + "in";
        past = "in" + word;
        present = "in" + word.substring(0,1) + word;
        future = word.substring(0,1) + word + "in";
    },

    isLY: function(word) {
        let letter = word.substring(0,1);
        if (letter == 'l' || letter == 'y') {
            return true;
        }
        return false;
    },

    doLY: function(word) {
        cmd = word + "in";
        past = "ni" + word;
        present = "ni" + word.substring(0,2) + word;
        future = word.substring(0,1) + word + "in";
    },
    
    In: function(word) {
        // TODO: Check if the second to last letter is 'o' and change to 'u'

        // Check when '-hin' should be used
        // Check when words like 'gawin' exist
        if (noLastLetter.includes(word)) {
            if (nonGlottal.includes(word)) {
                cmd = word.substring(0, word.length - 1) + "hin";
            }
            else {
                cmd = word.substring(0, word.length - 1) + "in";
            }
        }
        else if (nonGlottal.includes(word)) {
            cmd = word.substring(0, word.length - 1) + "hin";
        }
        else {
            cmd = word + "in";
        }
        
        past = word.substring(0,1) + "in" + word.substring(1);
        present = word.substring(0,1) + "in" + word.substring(1,2) + word;

        if (noLastLetter.includes(word)) {
            if (nonGlottal.includes(word)) {
                future = word.substring(0,2) + word.substring(0, word.length - 1) + "hin";
            }
            else {
                future = word.substring(0,2) + word.substring(0, word.length - 1) + "in";
            }
        }
        else if (nonGlottal.includes(word)) {
            future = word.substring(0,2) + word.substring(0, word.length - 1) + "hin";
        }
        else {
            future = word.substring(0,2) + word + "in";
        }
        
    },
    
    conjugate: function(word) {
        if (functions.isLY(word)) {
            functions.doLY(word);
        }
        else if (functions.isVowel(word)) {
            functions.doVowel(word);
        }
        else {
            functions.In(word);
        }
        
        return [
            {
                tense: "Command",
                conjugation: cmd
            },
            {
                tense: "Past",
                conjugation: past
            },
            {
                tense: "Present",
                conjugation: present
            },
            {
                tense: "Future",
                conjugation: future
            }
        ]
    }
};

export default functions;